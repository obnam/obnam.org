#!/bin/bash

set -euo pipefail

die()
{
    echo "$@" 1>&2
    exit 1
}

current_branch()
{
    git rev-parse --abbrev-ref HEAD
}

uncommitted () {
    git status --short | grep '^ M ' > /dev/null
}

case "$(current_branch)" in
    master|main)
	if uncommitted
	then
	    die "there are uncommitted change"
	fi
	git pull --rebase gitlab HEAD
	git push origin HEAD
	;;
    *)
	die "not in main or master branch"
	;;
esac
