[[!meta title="Iteration planning: May 10&mdash;May 30"]]
[[!tag meeting]]

[[!toc levels=1]]

# Assessment of the iteration that has ended

[previous iteration]: /blog/2021/04/25/meeting

The goal for the [previous iteration][] was:

> The main goal of this iteration is to add at least rudimentary
> encryption of chunks, before they’re uploaded to the server, and
> decryption and validation after they’re downloaded. This should use
> the encryption keys stored by `obnam init`. This is done only if the
> client configuration says encryption is turned on, to allow an opt-in
> approach to encryption for now. Later on, encryption won’t be
> optional.
>
> Additionally, work will continue on using iterators instead of
> potentially enormous vectors when querying the SQLite database.

This goal was not reached. Lars is looking for work, which takes a lot
of energy, and doesn't leave much for Obnam.


# Discussion

## Lars

Lars won't commit to anything this iteration, but will do what he can
to review and comment on other things.

## GitLab CI

In [[!mr 143]], Alexander Batischev has started work to set up GitLab
CI for Obnam. Lars needs to learn more about this before he can merge
it, but will try to get to that soon.

Having CI for merge requests would be a boon. Building and publishing
Debian packages and websites will remain Lars's personal CI for now.

## Logo

In [issue #7 for
obnam.org](https://gitlab.com/obnam/obnam.org/-/issues/7),
Tigran Zakoyan has proposed possible new logos for Obnam. Feedback on
those would be welcome.


# Goals

## Goal for 1.0 (not changed this iteration)

The goal for version 1.0 is for Obnam to be an utterly boring backup
solution for Linux command line users. It should just work, be
performant, secure, and well-documented.

It is not a goal for version 1.0 to have been ported to other
operating systems, but if there are volunteers to do that, and to
commit to supporting their port, ports will be welcome.

Other user interfaces is likely to happen only after 1.0.

The server component will support multiple clients in a way that
doesn’t let them see each other’s data. It is not a goal for clients
to be able to share data, even if the clients trust each other.

## Goal for the next few iterations (not changed for this iteration)

The goal for next few iterations is to have Obnam support encryption
well. This will involve having a documented threat model, which has
been reviewed by all stakeholders participating in the project, and
Obnam defending against all the modeled threats.

## Goal for the iteration that is starting

There is no goal for this iteration.

# Commitments for this iteration

There are no commitments for this iteration.


# Meeting participants

* Lars Wirzenius
