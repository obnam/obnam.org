[[!meta title="Iteration planning: June 6&ndash;19"]]
[[!tag meeting]]

[[!toc levels=1]]

# Assessment of the iteration that has ended

[previous iteration]: /blog/2021/05/10/meeting

There was no goal for the [previous iteration][], due to Lars being
busy with life and looking for work.

However, Alexander Batischev changed Obnam to make use of GitLab CI,
which will help catch more problems earlier. Also, the logo by Tigran
Zakoyan has been accepted and put onto the front page of the Obnam web
site. Lars finished up the first version of encryption support.


# Discussion

## Feature development work

Lars has been thinking. There are many fundamental,
necessary features of Obnam that need to be implemented, and to avoid
complicating things, it's probably best to work on one at a time. The
order matters: the earlier a feature gets some real use, the earlier
it gets feedback about what's good and what needs improvement.
That feedback is necessary to guide further development. The approach
proposed by Lars is to work on one big feature at a time, for an
iteration or a few, then switch to another. This will get features
into the hands of users earlier, without complicating implementation,
when features inevitably conflict.

At the same time, any bugs that are found should be fixed, and the
backlog of bugs should also be worked on. It doesn't do to have a list
of know bugs in the long run.

The headline features include:

* encryption (first version is there now)
* performance
* fine-grained de-duplication
* securely sharing the server between clients

Of these, performance is the currently most urgent, as it's making it
inconvenient for Lars to use Obnam for his own backups.

## GitLab.com organisation

The two Obnam git repositories, for code and website, are currently
mixed among all of Lars's personal repositories. This is a little
confusing to others. It would be nice to create an "organisation" on
gitlab.com for Obnam. That will mean some fixing in CI and web pages
that link to the current location, but it's better done earlier
rather than later.

# Goals

## Goal for 1.0 (not changed this iteration)

The goal for version 1.0 is for Obnam to be an utterly boring backup
solution for Linux command line users. It should just work, be
performant, secure, and well-documented.

It is not a goal for version 1.0 to have been ported to other
operating systems, but if there are volunteers to do that, and to
commit to supporting their port, ports will be welcome.

Other user interfaces is likely to happen only after 1.0.

The server component will support multiple clients in a way that
doesn’t let them see each other’s data. It is not a goal for clients
to be able to share data, even if the clients trust each other.

## Goal for the next few iterations (new for this iteration)

The goal for next few iterations is to have Obnam be performant. This
will include, at least, making the client use more concurrency so that
it can use more CPU cores to compute checksums for de-duplication.

## Goal for this iteration

Change the Obnam client to use async Rust to be more concurrent. The
result doesn't need to make optimal use of all cores, but it needs to
pass the test suite and needs to work.

# Commitments for this iteration

Lars intends to work on the following issues (rough estimates given):

* [[!issue 96]] (email backup benchmark) &ndash; 1h
* [[!issue 109]] (make a release) &ndash; 1h
* [[!issue 113]] (client is not async) &ndash; 4h

The async change is fundamental for the goal of the next few
iterations. The benchmark is needed to see if the async change has any
effect. The release is needed to get code into the hands of others.

If there's time, Lars will also work on the following:

* [[!issue 111]] (document what Subplot needs) &ndash; 0.25h
* [[!issue 116]] (obnam org on gitlab.com) &ndash; 1h

That's a total of 6h + 1.25h of estimated effort.

Alexander Batischev intends to make any necessary changes to the
GitLab CI setup, such as a Docker proxy, if Obnam gets its own
organisation on gitlab.com ([[!issue 116]])

# Meeting participants

* Alexander Batischev
* Lars Wirzenius
