[[!meta title="Iteration planning: November"]]
[[!meta date="Sat, 05 Nov 2022 08:30:24 +0200"]]
[[!tag meeting]]

[[!toc levels=1]]

# Assessment of the iteration that has ended

[previous iteration]: /blog/2022/10/planning

The goal of the [previous iteration][] was:

> The goal of this iteration is to restart Obnam development.

The following issues were chosen for this iteration:

* **DONE:** [[!issue 208]] -- _Needs internal interface for using chunk storage_
* **DONE:** [[!issue 209]] -- _Uses structopt instead of clap v3 for command
  line parsing_

The goal was met.

# Discussion

The less frantic meeting schedule seems to work, for now. We can
increase the frequency again once Obnam development picks up momentum,
if we feel it's needed and feasible.

# Repository review

Lars reviewed all the open issues, merge requests, and CI pipelines
for all the projects in the [Obnam group on
gitlab.com](https://gitlab.com/obnam/).

|Project|issues|MRs|branches|CI|
|:-|-:|-:|-:|-:|
|[obnam](https://gitlab.com/obnam/obnam)|50|0|0|OK|
|[obnam.org](https://gitlab.com/obnam/obnam.org)|0|0|0|none|
|[Container Images](https://gitlab.com/obnam/container-images)|0|0|0|OK|
|[obnam-benchmark](https://gitlab.com/obnam/obnam-benchmark)|11|0|0|none|
|[cachedir](https://gitlab.com/obnam/cachedir)|0|0|0|none|
|[summain](https://gitlab.com/obnam/summain)|0|0|0|none|


# Goals

## Goal for 1.0 (not changed this iteration)

The goal for version 1.0 is for Obnam to be an utterly boring backup
solution for Linux command line users. It should just work, be
performant, secure, and well-documented.

It is not a goal for version 1.0 to have been ported to other
operating systems, but if there are volunteers to do that, and to
commit to supporting their port, ports will be welcome.

Other user interfaces is likely to happen only after 1.0.

The server component will support multiple clients in a way that
doesn’t let them see each other’s data. It is not a goal for clients
to be able to share data, even if the clients trust each other.

## Goal for the next few iterations (not changed for this iteration)

The goal for next few iterations is to have Obnam be easier and safer
to change, both for developers and end users. This means that
developers need to be able to make breaking changes without users
having to suffer. User shall be able to migrate their data, when they
feel it worthwhile, not just because there is a new version.

## Goal for this iteration (new for this iteration)

The goal of this iteration is to allow migration of a backup
repository to a new, incompatible version of Obnam or its server.


# Commitments for this iteration

Lars intends to work on the following issues:

* [[!issue 46]] -- _Lacks an export/import function_
  - estimate: 8h

# Meeting participants

* Lars Wirzenius
