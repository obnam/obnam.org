[[!meta title="Iteration planning: March 6&ndash;20"]]
[[!meta date="Wed, 23 Feb 2022 18:52:11 +0200"]]
[[!tag meeting]]

[[!toc levels=2]]

# Assessment of the iteration that has ended

[previous iteration]: /blog/2022/02/23/planning

The goal of the [previous iteration][] was:

> The goal for this iteration is to tidy up database abstraction code
> in the Obnam client and implement the performance improvements Lars
> did prototype code for.

This was completed, apart from actually merging the change, ahead of
time, so the iteration is cut short.

# Discussion

## Current development theme

The current theme of development for Obnam is **convenience**. The choices
are performance, security, convenience, and tidy-up, at least
currently.

Tidy-up is about tying up loose ends, fix minor issues, refactoring
code, and otherwise making things better, more tidy, without much
impact on end users.


# Repository review

Lars reviewed all the open issues, merge requests, and CI pipelines
for all the projects in the Obnam group on gitlab.com.

## [Container Images](https://gitlab.com/obnam/container-image)

* Open issues: 0
* Merge requests: 0
* Additional branches: 0
* CI: OK

A scheduled pipeline now rebuilds the images automatically every week,
so there's no need to manually trigger this for each iteration.

## [obnam.org](https://gitlab.com/obnam/obnam.org)

* Open issues: 0
* Merge requests: 0
* Additional branches: 0
* CI: not defined

## [obnam-benchmark](https://gitlab.com/obnam/obnam-benchmark)

* Open issues: 11
* Merge requests: 0
* Additional branches: 0
* CI: not defined

## [summain](https://gitlab.com/obnam/summain)

* Open issues: 0.
* Merge requests: 0.
* Additional branches: 0.
* CI: OK.

 There were no open issues,
no extra branches, and no open merge requests. There is no CI for this
repository.

## [obnam](https://gitlab.com/obnam/obnam)

* Open issues: 51
* Merge requests: 2
* Additional branches: 0
* CI: OK

Lars added the
[`schema-change`](https://gitlab.com/obnam/obnam/-/issues?sort=created_date&state=opened&label_name[]=schema-change)
label to [[!issue 192]] and [[!issue 193]].

# Goals

## Goal for 1.0 (not changed this iteration)

The goal for version 1.0 is for Obnam to be an utterly boring backup
solution for Linux command line users. It should just work, be
performant, secure, and well-documented.

It is not a goal for version 1.0 to have been ported to other
operating systems, but if there are volunteers to do that, and to
commit to supporting their port, ports will be welcome.

Other user interfaces is likely to happen only after 1.0.

The server component will support multiple clients in a way that
doesn’t let them see each other’s data. It is not a goal for clients
to be able to share data, even if the clients trust each other.

## Goal for the next few iterations (new for this iteration)

The goal for next few iterations is to have Obnam be easier and safer
to change, both for developers and end users. This means that
developers need to be able to make breaking changes without users
having to suffer. User shall be able to migrate their data, when they
feel it worthwhile, not just because there is a new version.

## Goal for this iteration (new for this iteration)

The goal for this iteration is to prepare for future schema changes.

# Commitments for this iteration

Lars will work on Obnam client database abstractions and performance.
The goal for these is for Obnam to be able to run `obnam backup` on a
live data set of a million files that haven't changed since the
previous backup in less than 30 seconds, on Lars's development server.

Lars intends to work on the following issues:

* [[!issue 42]] -- _Chunk checksums are in cleartext_
  - rename the "sha256" chunk metadata field to "label"
  - on the server, treat "sha256" as if it said "label"
  - this doesn't fix the issue, merely paves way for a fix
  - 1h
* [[!issue 153]] -- _Depends on vulnerable chrono_
  - replace use of `chrono` with the `time` crate: we only use it to
    generate a timestamp and `time` can do that now
  - 1h
* [[!issue 19]] -- _Doesn't document all the metadata that various
  systems have and what Obnam does about them_
  - document what is known to be missing, then close the issue with a
    note that any other things that may be missing should be reported
    as new issues
  - 1h
* [[!issue 194]] -- _Can only handle one backup schema version at a
  time_
  - this includes [[!issue 192]] -- _"fileno" in the SQL schema should
    be "fileid"_
  - 4h

# Meeting participants

* Lars Wirzenius
